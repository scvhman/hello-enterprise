package com.scvh.apps.hello.enterprise.abstractions

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "test")
data class Hello(@Id @Column(name = "first") val hello: String, @Column(name = "second") val enterprise: String)