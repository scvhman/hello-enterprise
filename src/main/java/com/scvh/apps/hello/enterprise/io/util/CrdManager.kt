package com.scvh.apps.hello.enterprise.io.util

import com.scvh.apps.hello.enterprise.io.crd.DatabaseDeleter
import com.scvh.apps.hello.enterprise.io.crd.DatabaseReader
import com.scvh.apps.hello.enterprise.io.crd.DatabaseWrite
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
open class CrdManager(@Autowired val reader: DatabaseReader, @Autowired val writer: DatabaseWrite, @Autowired val deleter: DatabaseDeleter) {

    fun write(key: String, value: String) {
        writer.databaseWrite(key, value)
    }
}