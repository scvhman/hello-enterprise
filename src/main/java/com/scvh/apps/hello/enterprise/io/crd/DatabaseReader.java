package com.scvh.apps.hello.enterprise.io.crd;


import com.scvh.apps.hello.enterprise.abstractions.Hello;
import com.scvh.apps.hello.enterprise.io.util.DatabaseConnectivityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DatabaseReader {

    private DatabaseConnectivityManager connectivityManager;

    @Autowired
    public void setConnectivityManager(DatabaseConnectivityManager connectivityManager) {
        this.connectivityManager = connectivityManager;
    }

    public Hello readFromDatabase(String key) {
        Session session = connectivityManager.openSession();
        Hello hello = session.get(Hello.class, key);
        session.close();
        return hello;
    }
}
