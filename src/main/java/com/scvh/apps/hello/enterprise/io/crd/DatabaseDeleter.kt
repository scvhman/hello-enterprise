package com.scvh.apps.hello.enterprise.io.crd

import com.scvh.apps.hello.enterprise.io.util.DatabaseConnectivityManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
open class DatabaseDeleter(@Autowired val manager: DatabaseConnectivityManager) {

    fun dbRemove(key: String) {
        val session = manager.openSession()
        session.delete(key)
        session.close()
    }
}