package com.scvh.apps.hello.enterprise.io.crd

import com.scvh.apps.hello.enterprise.abstractions.Hello
import com.scvh.apps.hello.enterprise.io.util.DatabaseConnectivityManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
open class DatabaseWrite(@Autowired val manager: DatabaseConnectivityManager) {

    fun databaseWrite(toWrite: String, toWrite2: String) {
        val session = manager.openSession()
        session.beginTransaction()
        session.save(Hello(toWrite, toWrite2))
        session.close()
    }
}