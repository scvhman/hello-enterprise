package com.scvh.apps.hello.enterprise.io.util;

import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Component;

@Component
class DatabaseConfigManager {

    Configuration createCfg() {
        return new Configuration()
                .setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect")
                .setProperty("hibernate.connection.username", "test")
                .setProperty("hibernate.connection.password", "test")
                .setProperty("hibernate.connection.url", "jdbc:mysql://localhost:3306/db")
                .setProperty("hibernate.order_updates", "true");
    }
}
