package com.scvh.apps.hello.enterprise.io.util;

import com.scvh.apps.hello.enterprise.abstractions.Hello;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DatabaseConnectivityManager {

    private DatabaseConfigManager configManager;

    @Autowired
    public void setConfigManager(DatabaseConfigManager configManager) {
        this.configManager = configManager;
    }

    public Session openSession() {
        return configManager.createCfg().addAnnotatedClass(Hello.class).buildSessionFactory()
                .openSession();
    }
}
