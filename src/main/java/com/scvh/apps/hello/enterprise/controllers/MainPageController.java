package com.scvh.apps.hello.enterprise.controllers;

import com.scvh.apps.hello.enterprise.abstractions.Hello;
import com.scvh.apps.hello.enterprise.helpers.StatementPrepare;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainPageController {

    private StatementPrepare prepare;

    @Autowired
    public void setPrepare(StatementPrepare prepare) {
        this.prepare = prepare;
    }

    @RequestMapping("/")
    public Hello front(@RequestParam(value = "first", required = true) String firstParam,
                       @RequestParam(value = "second", required = true) String secondParam) {
        return prepare.prepare(firstParam, secondParam);
    }
}
