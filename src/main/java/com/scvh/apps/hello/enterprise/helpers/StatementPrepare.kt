package com.scvh.apps.hello.enterprise.helpers

import com.scvh.apps.hello.enterprise.abstractions.Hello
import com.scvh.apps.hello.enterprise.io.util.CrdManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
open class StatementPrepare(@Autowired val crd: CrdManager) {

    fun prepare(input: String, input2: String): Hello {
        crd.write(input, input2)
        return Hello(input, input2)
    }
}